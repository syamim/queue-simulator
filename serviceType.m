function output = serviceType(x)
    printf('\nService Type probability table\n');
    printf('--------------------------------------------------------|\n');
    printf('| Service Type | Probability  |    CDF      |  Range    |\n');
    printf('--------------------------------------------------------|\n');
    printf('|      1       |   0.60       |    0.30     |  1 - 60   |\n');
    printf('|      2       |   0.40       |    0.60     | 61 - 100  |\n');
    printf('--------------------------------------------------------|\n');