function output = serviceTime(x)
    printf('\nTypes of Service offered\n');
    printf('---------------------------------------------------------------------\n');
    printf('| Service Type |               Detail                   | Counter   |\n');
    printf('---------------------------------------------------------------------\n');
    printf('|      1       | Normal Case / Age Below 60             | 1,2       |\n');
    printf('|      2       | Emergency Case / Age 60 and abov       | 3         |\n');
    printf('---------------------------------------------------------------------\n');
    for i = 1:5
        if i == 1
           ST(i) = 3; 
           prob(i) = 0.1;
           CDF(i) = prob(i);
           firstNum(i) = 1;
           lastNum(i) = CDF(i)*100;
        elseif i == 2
           ST(i) = 4; 
           prob(i) = 0.15;
           CDF(i) = CDF(i-1)+prob(i);
           firstNum(i) = lastNum(i-1)+1;
           lastNum(i) = CDF(i)*100;
        elseif i == 3
           ST(i) = 5; 
           prob(i) = 0.25;
           CDF(i) = CDF(i-1)+prob(i);
           firstNum(i) = lastNum(i-1)+1;
           lastNum(i) = CDF(i)*100;
        elseif i == 4
           ST(i) = 6; 
           prob(i) = 0.3;
           CDF(i) = CDF(i-1)+prob(i);
           firstNum(i) = lastNum(i-1)+1;
           lastNum(i) = CDF(i)*100;
        elseif i == 5
           ST(i) = 7; 
           prob(i) = 0.2;
           CDF(i) = CDF(i-1)+prob(i);
           firstNum(i) = lastNum(i-1)+1;
           lastNum(i) = CDF(i)*100;
        end
     end 
     printf('\nNormal Case Service Time\n');
     printf('----------------------------------------------------\n');
     printf('| Service Time  |  Probability  |  CDF   | Range   |\n');
     printf('----------------------------------------------------\n');
     for i = 1:5
         printf('|      %3.0f      |    %1.2f       |  %1.2f  |%3.0f-%3.0f  |\n', ST(i), prob(i), CDF(i), firstNum(i), lastNum(i));
     end
     printf('----------------------------------------------------\n');
     
     for i = 1:5
        if i == 1
           ST(i) = 6; 
           prob(i) = 0.15;
           CDF(i) = prob(i);
           firstNum(i) = 1;
           lastNum(i) = CDF(i)*100;
        elseif i == 2
           ST(i) = 7; 
           prob(i) = 0.30;
           CDF(i) = CDF(i-1)+prob(i);
           firstNum(i) = lastNum(i-1)+1;
           lastNum(i) = CDF(i)*100;
        elseif i == 3
           ST(i) = 8; 
           prob(i) = 0.25;
           CDF(i) = CDF(i-1)+prob(i);
           firstNum(i) = lastNum(i-1)+1;
           lastNum(i) = CDF(i)*100;
        elseif i == 4
           ST(i) = 9; 
           prob(i) = 0.1;
           CDF(i) = CDF(i-1)+prob(i);
           firstNum(i) = lastNum(i-1)+1;
           lastNum(i) = CDF(i)*100;
        elseif i == 5
           ST(i) = 10; 
           prob(i) = 0.2;
           CDF(i) = CDF(i-1)+prob(i);
           firstNum(i) = lastNum(i-1)+1;
           lastNum(i) = CDF(i)*100;
        end
     end 
     printf('\nEmergency Case Service Time\n');
     printf('----------------------------------------------------\n');
     printf('| Service Time  |  Probability  |  CDF   | Range   |\n');
     printf('----------------------------------------------------\n');
     for i = 1:5
         printf('|      %3.0f      |    %1.2f       |  %1.2f  |%3.0f-%3.0f  |\n', ST(i), prob(i), CDF(i), firstNum(i), lastNum(i));
     end
     printf('----------------------------------------------------\n');
 end