function output = interarrival(x) % Inter-Arrival Time Table
    for i = 1:5
        if i == 1
            intArrTime(i) = 1;
            prob(i) = 0.3;
            CDF(i) = prob(i);
            firstNum(i) = 1;
            lastNum(i) = CDF(i)*100;
        elseif i == 2
            intArrTime(i) = 2;
            prob(i) = 0.25;
            CDF(i) = CDF(i-1)+prob(i);
            firstNum(i) = lastNum(i-1)+1;
            lastNum(i) = CDF(i)*100;
        elseif i == 3
            intArrTime(i) = 3;
            prob(i) = 0.2;
            CDF(i) = CDF(i-1)+prob(i);
            firstNum(i) = lastNum(i-1)+1;
            lastNum(i) = CDF(i)*100;
        elseif i == 4
            intArrTime(i) = 4;
            prob(i) = 0.1;
            CDF(i) = CDF(i-1)+prob(i);
            firstNum(i) = lastNum(i-1)+1;
            lastNum(i) = CDF(i)*100; 
        else
            intArrTime(i) = 5;
            prob(i) = 0.15;
            CDF(i) = CDF(i-1)+prob(i);
            firstNum(i) = lastNum(i-1)+1;
            lastNum(i) = CDF(i)*100;       
         end
    end
    
    %Display table
    
    printf('\nInterarrival Time\n');
    printf('-------------------------------------------------------\n');
    printf('| Inter-Arrival Time | Probability |  CDF  |   Range  |\n');
    printf('-------------------------------------------------------\n');
    for i=1:5
        printf('|        %4.0f        |     %1.2f    |  %1.2f | %3.0f-%3.0f  |\n', intArrTime(i), prob(i), CDF(i), firstNum(i), lastNum(i));
    end
    printf('-------------------------------------------------------\n');
    
            

