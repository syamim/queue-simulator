function output = mainSimulator()
    serviceType();
    serviceTime();
    interarrival();
    
    patientNumber = input('How many patients are there? Minimum 1 patient: ');
    % Counter Setting
    normalCounterNum1 = 1; % normal counter number
    normalCounterNum2 = 2; % normal counter number
    priority_counterNum = 3;
    % End Counter Setting
    
    % Get random generator algo
    printf('Which random generator would you choose?\n');
    printf('1.Linear Congruential Generator\n2.Uniformly Distributed Integer Generator\n3.Floor Generator\n');
    algoChoice = input('Enter your choice : ');
    % END Get random generator algo
    
    % TESTING PURPOSE
    % algoChoice = 1;
    % patientNumber = 20;
    % END TESTING PURPOSE
    
    % Generating random number for interval and service type
    if (algoChoice == 1) % Linear Congruential Generator      
        for i = 1:patientNumber
            patientRandCaseType(i) = round(mod(rand.*100,99)+1);
            patientRandInterval(i) = round(mod(rand.*100,99)+1);
        end
    elseif(algoChoice == 2) % Uniformly Distributed Integer
        for i = 1:patientNumber
            patientRandCaseType(i) = randi(1,100);
            patientRandInterval(i) = round(mod(rand.*100,99)+1);
        end
    elseif(algoChoice == 3) % Floor random generator
        for i = 1:patientNumber
            patientRandCaseType(i) = floor(99.*rand(1)+1);
            patientRandInterval(i) = round(mod(rand.*100,99)+1);
        end
    end
     
    % Case type based on random number
    for i = 1:patientNumber
        if (patientRandCaseType(i) >= 1 & patientRandCaseType(i) <= 60)
            patientCaseType(i) = 1;                       
        elseif(patientRandCaseType(i) >= 61 & patientRandCaseType(i) <= 100)
            patientCaseType(i) = 2;         
        end
    end
                   
    % Inter-arrival time based on random number
    for i = 1:patientNumber
        if (patientRandInterval(i) >= 1 && patientRandInterval(i) <= 30)
            patientInterval(i) = 1;
        elseif (patientRandInterval(i) >= 31 && patientRandInterval(i) <= 55)
            patientInterval(i)=2;
        elseif (patientRandInterval(i) >= 56 && patientRandInterval(i) <= 75)
            patientInterval(i)=3;
        elseif (patientRandInterval(i) >= 76 && patientRandInterval(i) <= 85)
            patientInterval(i)=4;
        elseif (patientRandInterval(i) >= 86 && patientRandInterval(i) <= 100)
            patientInterval(i)=5;
        end
    end
    
    % Generate random age
    for i = 1:patientNumber
        patientAge(i) = randi(1,100);
    end
    
    patientQueueNumberMain(1) = 0; % Customer Queue number time
    patientArrMain(1) = 0; % Customers arrival time    
    patientServiceMain(1) = 0; % Customer Service start time
    patientDepartMain(1) = 0; % Customer Depart time

    counter1=1;
    normalPatientArr(1) = 0;
    normalPatientNum = 0;
    normalPatientInd(1) = 0;
    normalQueueNum(1) = 1001; % might not use
    
    counter2=1;
    priorityPatientArr(1) = 0;
    priorityPatientNum = 0;
    priorityPatientInd(1) = 0;
    priorityQueueNum(1) = 2001; % might not use 
    
    patientRandInterval(1) = 0;
    patientInterval(1) = 0;
    printf('=================================================================================================\n');
    printf('=======================                      Simulation Start                     ===============\n');
    printf('=================================================================================================\n');
    printf('-------------------------------------------------------------------------------------------------\n');
    printf('|  n  | RN for Inter- | Interarrival | Arrival | Age | RNs for the  |    Case           | Queue  |\n');
    printf('|     | arrival time  |    Time      |  Time   |     | case type    |    Type           | number |\n');
    printf('-------------------------------------------------------------------------------------------------\n');
    for i = 1:patientNumber 
        if i ~= 1
            patientArrMain(i) = patientArrMain(i-1)+patientInterval(i);
        end
        
        if patientAge(i) >= 60
            priorityPatientNum = priorityPatientNum+1; % Calculate total patient for service 2
            if counter2 ~= 1
                priorityQueueNum(counter2) = priorityQueueNum(counter2-1)+1; % Accumulate queue number
            end
            printf('| %3.0f |    %4.0f       |   %4.0f       |  %4.0f   | %3.0f |  %4.0f        | Normal Case       |  %4.0f  |\n', i, patientRandInterval(i), patientInterval(i), patientArrMain(i), patientAge(i), patientRandCaseType(i), priorityQueueNum(counter2));
            priorityPatientArr(counter2) = patientArrMain(i); % save arrival time into array
            priorityPatientInd(counter2) = i; % save index to be used later
            counter2 = counter2+1;
        elseif patientCaseType(i) == 2 
            priorityPatientNum = priorityPatientNum+1; % Calculate total patient for service 2
            if counter2 ~= 1
                priorityQueueNum(counter2) = priorityQueueNum(counter2-1)+1; % Accumulate queue number
            end
            printf('| %3.0f |    %4.0f       |   %4.0f       |  %4.0f   | %3.0f |  %4.0f        | Emergency Case    |  %4.0f  |\n', i, patientRandInterval(i), patientInterval(i), patientArrMain(i), patientAge(i), patientRandCaseType(i), priorityQueueNum(counter2));
            priorityPatientArr(counter2) = patientArrMain(i); % save arrival time into array
            priorityPatientInd(counter2) = i; % save index to be used later
            counter2 = counter2+1;
        else
            normalPatientNum = normalPatientNum+1; % Calculate total patient for service 1
            if counter1 ~= 1
                normalQueueNum(counter1) = normalQueueNum(counter1-1)+1; % Accumulate queue number              
            end           
            printf('| %3.0f |    %4.0f       |   %4.0f       |  %4.0f   | %3.0f |  %4.0f        | Normal Case       |  %4.0f  |\n', i, patientRandInterval(i), patientInterval(i), patientArrMain(i), patientAge(i), patientRandCaseType(i), normalQueueNum(counter1));
            normalPatientArr(counter1) = patientArrMain(i); % save arrival time into array
            normalPatientInd(counter1) = i; % save index to be used later
            counter1 = counter1+1;
        end
    end
    printf('-------------------------------------------------------------------------------------------------\n');
   
    
    % Normal Cases Tabel (Counter 1 & 2)
    if normalPatientNum ~= 0

        if (algoChoice == 1) % Linear Congruential Generator      
            for i = 1:normalPatientNum
                normal_patientRandST(i) = round(mod(rand.*100,99)+1);   
            end  
        elseif (algoChoice == 2) % Uniformly Distributed Integer
            for i = 1:normalPatientNum
                normal_patientRandST(i) = randi(1,100);
            end
        elseif (algoChoice == 3)
            for i = 1:normalPatientNum
                normal_patientRandST(i) = floor(99.*rand(1)+1);
            end
        end
    
        for i = 1:normalPatientNum % Set Service time for service 1
            if(normal_patientRandST(i) >= 1 && normal_patientRandST(i) <= 10)
                normal_patientST(i)=3;
            elseif (normal_patientRandST(i)>=11 && normal_patientRandST(i)<=25)
                normal_patientST(i)=4;
            elseif (normal_patientRandST(i)>=26 && normal_patientRandST(i)<=50)
                normal_patientST(i)=5;
            elseif (normal_patientRandST(i)>=51 && normal_patientRandST(i)<=80)
                normal_patientST(i)=6;
            elseif (normal_patientRandST(i)>=81 && normal_patientRandST(i)<=100)
                normal_patientST(i)=7;
            end    
        end
    
        normal_serviceBegin(1) = normalPatientArr(1);
        normal_serviceEnd(1) = normal_serviceBegin(1) + normal_patientST(1);
        normal_serviceEndCounts = 1;
        normal_waitingTime(1) = 0;   
        normal_timeSpent(1) = normal_patientST(1) + normal_waitingTime(1);
        normal_queueFrequency = 0;
        normal_totalQueueFrequency = 0;
        normal_counter1StartTime = 0;
        
        normal_serviceEndCounter2(1) = 0;
        normal_serviceEndCounter2Counts = 1;
        normal_counter1StartTimeStatus = 1;
        normal_counter2Status = 0;    
        normal_counter2StartTime = 0;
        normal_counter2StartTimeStatus = 1;
        normal_counter2Num = 0;
        normal_counter1Num = 1;
        normal_waitingTimeCounter1(1) = 0;
        normal_waitingTimeCounter2(1) = 0;
        normal_timeSpentCounter1(1) = normal_timeSpent(1);
        normal_timeSpentCounter2(1) = 0;
        normal_serviceTimeCounter1(1) = normal_patientST(1);
        normal_serviceTimeCounter2(1) = 0;
        normal_frequencyCounter1 = 0;
        normal_frequencyCounter2 = 0;
        
        patientDepartMain(normalPatientInd(1)) = normal_serviceEnd(1);
        patientServiceMain(normalPatientInd(1)) = normal_serviceBegin(1);
        patientQueueNumberMain(normalPatientInd(1)) = normalQueueNum(1);
        patientCounterNumberMain(normalPatientInd(1)) = normalCounterNum1;

        printf('===================================================================================================\n');
        printf('===============                           Normal Case Counter                       ===============\n');
        printf('===================================================================================================\n');
        printf('---------------------------------------------------------------------------------------------------\n');
        printf('| Queue  | Counter | RN Service | Service | Time Service | Time Service | Waiting | Time spends in |\n');
        printf('| number | number  |   Time     |  Time   |    Begins    |     Ends     |  time   |   the system   |\n');
        printf('---------------------------------------------------------------------------------------------------\n');
        printf('|  %4.0f  | %4.0f    |    %4.0f    |  %4.0f   |     %4.0f     |     %4.0f     |  %4.0f   |      %4.0f      |\n', normalQueueNum(1), normalCounterNum1, normal_patientRandST(1), normal_patientST(1), normal_serviceBegin(1), normal_serviceEnd(1), normal_waitingTime(1), normal_timeSpent(1));
        for i = 2:normalPatientNum
            normal_difference1 = normal_serviceEnd(normal_serviceEndCounts) - normalPatientArr(i);
            normal_difference2 = normal_serviceEndCounter2(normal_serviceEndCounter2Counts) - normalPatientArr(i);
            if (normal_difference2 < normal_difference1 & normal_serviceEnd(normal_serviceEndCounts) > normalPatientArr(i)) % utk selang selikan counter
                normal_counter2Num = normal_counter2Num+1; 
                normal_serviceEndCounter2Counts = normal_serviceEndCounter2Counts+1;
                normal_waitingTime(i) = 0;
                % calculate waiting time                
                if normalPatientArr(i) < normal_serviceEndCounter2(normal_serviceEndCounter2Counts - 1)
                    normal_waitingTime(i) = normal_serviceEndCounter2(normal_serviceEndCounter2Counts - 1) - normalPatientArr(i);
                    normal_totalQueueFrequency = normal_totalQueueFrequency+1; % Calculate Average
                    normal_frequencyCounter2 = normal_frequencyCounter2 + 1;
                end                                                             
                normal_serviceBegin(i) = normalPatientArr(i) + normal_waitingTime(i);
                normal_serviceEndCounter2(normal_serviceEndCounter2Counts) = normal_serviceBegin(i) + normal_patientST(i);
                normal_timeSpent(i) = normal_waitingTime(i) + normal_patientST(i);
                normal_waitingTimeCounter2(normal_counter2Num) = normal_waitingTime(i);
                normal_timeSpentCounter2(normal_counter2Num) = normal_timeSpent(i);
                normal_serviceTimeCounter2(normal_counter2Num) = normal_patientST(i);

                % Data for logs event
                patientDepartMain(normalPatientInd(i)) = normal_serviceEndCounter2(normal_serviceEndCounter2Counts);
                patientServiceMain(normalPatientInd(i)) = normal_serviceBegin(i);
                patientQueueNumberMain(normalPatientInd(i)) = normalQueueNum(i);
                patientCounterNumberMain(normalPatientInd(i)) = normalCounterNum2;
                if normal_counter2StartTimeStatus == 1
                    normal_counter2StartTime = normal_serviceBegin(i);
                    normal_counter2StartTimeStatus = 0;
                end
                % END Data for logs event
                printf('|  %4.0f  | %4.0f    |    %4.0f    |  %4.0f   |     %4.0f     |     %4.0f     |  %4.0f   |      %4.0f      |\n', normalQueueNum(i), normalCounterNum2, normal_patientRandST(i), normal_patientST(i), normal_serviceBegin(i), normal_serviceEndCounter2(normal_serviceEndCounter2Counts), normal_waitingTime(i), normal_timeSpent(i));
            else
                normal_serviceEndCounts = normal_serviceEndCounts+1;                                   
                normal_waitingTime(i) = 0;
                normal_counter1Num = normal_counter1Num+1;
                if normalPatientArr(i) < normal_serviceEnd(normal_serviceEndCounts-1)
                    normal_queueFrequency = normal_queueFrequency+1;                   
                    normal_waitingTime(i) = normal_serviceEnd(normal_serviceEndCounts-1)-normalPatientArr(i);
                    normal_totalQueueFrequency = normal_totalQueueFrequency+1;
                    normal_frequencyCounter1 = normal_frequencyCounter1 + 1;
                end                                                             
                normal_serviceBegin(i) = normalPatientArr(i)+normal_waitingTime(i);
                normal_serviceEnd(normal_serviceEndCounts) = normal_serviceBegin(i)+normal_patientST(i);
                normal_timeSpent(i) = normal_waitingTime(i)+normal_patientST(i);
                normal_waitingTimeCounter1(normal_counter1Num) = normal_waitingTime(i);
                normal_timeSpentCounter1(normal_counter1Num) = normal_timeSpent(i);
                normal_serviceTimeCounter1(normal_counter1Num) = normal_patientST(i);
        
                % Data for logs event
                patientDepartMain(normalPatientInd(i)) =  normal_serviceEnd(normal_serviceEndCounts);
                patientServiceMain(normalPatientInd(i)) = normal_serviceBegin(i);
                patientQueueNumberMain(normalPatientInd(i)) = normalQueueNum(i);
                patientCounterNumberMain(normalPatientInd(i)) = normalCounterNum1;
                if normal_counter1StartTimeStatus == 1
                    normal_counter1StartTime = patientServiceMain(normalPatientInd(1));
                    normal_counter1StartTimeStatus = 0;
                end
                % END Data for logs event
                printf('|  %4.0f  | %4.0f    |    %4.0f    |  %4.0f   |     %4.0f     |     %4.0f     |  %4.0f   |      %4.0f      |\n',normalQueueNum(i) ,normalCounterNum1 ,normal_patientRandST(i), normal_patientST(i), normal_serviceBegin(i) ,normal_serviceEnd(normal_serviceEndCounts) ,normal_waitingTime(i), normal_timeSpent(i));
            end
        end
        printf('---------------------------------------------------------------------------------------------------\n\n');
    end
    % END Normal Cases Tabel (Counter 1 & 2)

    
    % Priority Case Table (Counter 3)
    if priorityPatientNum ~= 0
        if (algoChoice == 1) % Linear Congruential Generator      
            for i = 1:priorityPatientNum
                priority_patientRandST(i) = round(mod(rand.*100,99)+1);   
            end  
        elseif(algoChoice == 2) % Uniformly Distributed Integer
            for i = 1:priorityPatientNum
                priority_patientRandST(i) = randi(1,100);
            end
        elseif(algoChoice == 3)
            for i = 1:priorityPatientNum
                priority_patientRandST(i) = floor(99.*rand(1)+1);
            end
        end
    
        for i = 1:priorityPatientNum % Service time for priority case
            if (priority_patientRandST(i) >= 1 && priority_patientRandST(i) <= 15)
                priority_patientST(i) = 6;
            elseif (priority_patientRandST(i) >= 16 && priority_patientRandST(i) <= 45)
                priority_patientST(i) = 7;
            elseif (priority_patientRandST(i) >= 46 && priority_patientRandST(i) <= 70)
                priority_patientST(i) = 8;
            elseif (priority_patientRandST(i) >= 71 && priority_patientRandST(i) <= 80)
                priority_patientST(i) = 9;
            elseif (priority_patientRandST(i) >= 81 && priority_patientRandST(i) <= 100)
                priority_patientST(i) = 10;
            end    
        end
        
        priority_serviceBegin(1) = priorityPatientArr(1);
        priority_serviceEnd(1) = priority_serviceBegin(1) + priority_patientST(1);
        priority_serviceEndCounter(1) = 0;
        priority_serviceEndCounterCounts = 1;
        priority_waitingTime(1) = 0;
        priority_timeSpent(1) = priority_patientST(1) + priority_waitingTime(1);
        priority_totalQueueFrequency = 0; % to find average

        patientDepartMain(priorityPatientInd(1)) = priority_serviceEnd(1);
        patientServiceMain(priorityPatientInd(1)) = priority_serviceBegin(1);
        patientQueueNumberMain(priorityPatientInd(1)) = priorityQueueNum(1);
        patientCounterNumberMain(priorityPatientInd(1)) = priority_counterNum;
        
        printf('===================================================================================================\n');
        printf('===============                         Priority Case Counter                       ===============\n');
        printf('===================================================================================================\n');
        printf('---------------------------------------------------------------------------------------------------\n');
        printf('| Queue  | Counter | RN Service | Service | Time Service | Time Service | Waiting | Time spends in |\n');
        printf('| number | number  |   Time     |  Time   |    Begins    |     Ends     |  time   |   the system   |\n');
        printf('---------------------------------------------------------------------------------------------------\n');
        printf('|  %4.0f  | %4.0f    |    %4.0f    |  %4.0f   |     %4.0f     |     %4.0f     |  %4.0f   |      %4.0f      |\n', priorityQueueNum(1), priority_counterNum, priority_patientRandST(1), priority_patientST(1), priority_serviceBegin(1), priority_serviceEnd(1), priority_waitingTime(1), priority_timeSpent(1));

        for i = 2:priorityPatientNum        
            priority_serviceEndCounterCounts = priority_serviceEndCounterCounts+1;
                
            priority_waitingTime(i) = 0;  % reset waiting time to zero
            % calculate waiting time current patient base on previous patient
            if priorityPatientArr(i) < priority_serviceEndCounter(priority_serviceEndCounterCounts-1)
                priority_waitingTime(i) = priority_serviceEndCounter(priority_serviceEndCounterCounts-1) - priorityPatientArr(i);
            end                                                             
            priority_serviceBegin(i) = priorityPatientArr(i) + priority_waitingTime(i);
            priority_serviceEndCounter(priority_serviceEndCounterCounts) = priority_serviceBegin(i) + priority_patientST(i); % for log event
            priority_timeSpent(i) = priority_waitingTime(i) + priority_patientST(i);

            % Data for log event
            patientDepartMain(priorityPatientInd(i)) = priority_serviceEndCounter(priority_serviceEndCounterCounts);
            patientServiceMain(priorityPatientInd(i)) = priority_serviceBegin(i);
            patientQueueNumberMain(priorityPatientInd(i)) = priorityQueueNum(i);
            patientCounterNumberMain(priorityPatientInd(i)) = priority_counterNum2;
            % END Data for log event
            printf('|  %4.0f  | %4.0f    |    %4.0f    |  %4.0f   |     %4.0f     |     %4.0f     |  %4.0f   |      %4.0f      |\n', priorityQueueNum(i), priority_counterNum, priority_patientRandST(i), priority_patientST(i), priority_serviceBegin(i), priority_serviceEndCounter(priority_serviceEndCounterCounts), priority_waitingTime(i), priority_timeSpent(i));
        end
        printf('---------------------------------------------------------------------------------------------------\n\n');
    end
    % END Priority Case Table (Counter 3)
    
    printf('==============================================================\n');
    printf('===                       DATA REPORT                      ===\n');
    printf('==============================================================\n');
    if patientNumber ~= 1
        avgInterArr = 0;
        for i = 1:patientNumber
            avgInterArr = avgInterArr+patientInterval(i);
        end
        avgInterArr = avgInterArr/(patientNumber-1);
        printf('Average Inter-Arrival Time: %4.5f\n',avgInterArr);
    end
    
    if patientNumber ~= 1
        avgArrTime = 0;
        for i = 1:patientNumber
            avgArrTime = avgArrTime+patientArrMain(i);
        end
        avgArrTime = avgArrTime/patientNumber;
        printf('Average Arrival Time: %4.5f\n',avgArrTime);
    end

    if normalPatientNum ~= 0
        normal_avgWaitingTime = 0;
        for i = 1:normalPatientNum
            normal_avgWaitingTime = normal_avgWaitingTime+normal_waitingTime(i);
        end
        normal_avgWaitingTime = normal_avgWaitingTime/normalPatientNum;
        printf('Average waiting time for Normal Cases : %4.5f\n',normal_avgWaitingTime);
    end
    
    if normal_counter1Num ~= 0
        normal_avgWaitingTime1 = 0;
        for i = 1:normal_counter1Num
            normal_avgWaitingTime1 = normal_avgWaitingTime1+normal_waitingTimeCounter1(i);
        end
        normal_avgWaitingTime1 = normal_avgWaitingTime1/normal_counter1Num;
        printf('Average waiting time for Normal Cases at Counter 1 : %4.5f\n',normal_avgWaitingTime1);
    end
    
    if normal_counter2Num ~= 0
        normal_avgWaitingTime2 = 0;
        for i = 1:normal_counter2Num
            normal_avgWaitingTime2 = normal_avgWaitingTime2+normal_waitingTimeCounter2(i);
        end
        normal_avgWaitingTime2 = normal_avgWaitingTime2/normal_counter2Num;
        printf('Average waiting time for Normal Cases at Counter 2 : %4.5f\n',normal_avgWaitingTime2);
    end
    
    if priorityPatientNum ~= 0
        priority_avgWaitingTime = 0;
        for i = 1:priorityPatientNum
            priority_avgWaitingTime = priority_avgWaitingTime+priority_waitingTime(i);
        end
        priority_avgWaitingTime = priority_avgWaitingTime/priorityPatientNum;
        printf('Average waiting time for Priority Cases (Counter 3) : %4.5f\n',priority_avgWaitingTime);
    end
    
    if normalPatientNum ~= 0
        normal_avgTimeSpent = 0;
        for i = 1:normalPatientNum
            normal_avgTimeSpent = normal_avgTimeSpent+normal_timeSpent(i);
        end
        normal_avgTimeSpent = normal_avgTimeSpent/normalPatientNum;
        printf('Average time spent for Normal Cases : %4.5f\n',normal_avgTimeSpent);
    end

    if normal_counter1Num ~= 0
        normal_avgTimeSpent1 = 0;
        for i = 1:normal_counter1Num
            normal_avgTimeSpent1 = normal_avgTimeSpent1+normal_timeSpentCounter1(i);
        end
        normal_avgTimeSpent1 = normal_avgTimeSpent1/normal_counter1Num;
        printf('Average time spend for Normal Cases at Counter 1 : %4.5f\n',normal_avgTimeSpent1);
    end

    if normal_counter2Num ~= 0
        normal_avgTimeSpent2 = 0;
        for i = 1:normal_counter2Num
            normal_avgTimeSpent2 = normal_avgTimeSpent2+normal_timeSpentCounter2(i);
        end
        normal_avgTimeSpent2 = normal_avgTimeSpent2/normal_counter2Num;
        printf('Average time spend for Normal Cases at Counter 2 : %4.5f\n',normal_avgTimeSpent2);
    end
    
    if priorityPatientNum ~= 0
        priority_avgTimeSpent = 0;
        for i = 1:priorityPatientNum
            priority_avgTimeSpent = priority_avgTimeSpent+priority_timeSpent(i);
        end
        priority_avgTimeSpent = priority_avgTimeSpent/priorityPatientNum;
        printf('Average time spent for Priority Cases (Counter 3) : %4.5f\n',priority_avgTimeSpent);
    end
    
    if normalPatientNum ~= 0
        normal_probWaiting = 0;
        normal_probWaiting = normal_totalQueueFrequency/normalPatientNum;
        printf('Waiting probability for Normal Cases : %4.5f\n',normal_probWaiting);
    end
    
    if normal_counter1Num ~= 0
        normal_probWaiting = 0;
        normal_probWaiting = normal_frequencyCounter1/normal_counter1Num;
        printf('Waiting probability for Normal Cases at Counter 1 : %4.5f\n',normal_probWaiting);
    end
    
    if normal_counter2Num ~= 0
        normal_probWaiting = 0;
        normal_probWaiting = normal_frequencyCounter2/normal_counter2Num;
        printf('Waiting probability for Normal Cases at Counter 2 : %4.5f\n',normal_probWaiting);
    end
    
    if priorityPatientNum ~= 0
        priority_probWaiting = 0;
        priority_probWaiting = priority_totalQueueFrequency/priorityPatientNum;
        printf('Waiting probability for Priority Cases (Counter 3) : %4.5f\n',priority_probWaiting);
    end
    
    if normalPatientNum ~= 0
        normal_avgST = 0;
        for i = 1:normalPatientNum
            normal_avgST = normal_avgST+normal_patientST(i);
        end
        normal_avgST = normal_avgST/normalPatientNum;
        printf('Average Service time for Normal Cases : %4.5f\n',normal_avgST);
    end
    
    if normal_counter1Num ~= 0
        normal_avgST = 0;
        for i = 1:normal_counter1Num
            normal_avgST = normal_avgST+normal_serviceTimeCounter1(i);
        end
        normal_avgST = normal_avgST/normal_counter1Num;
        printf('Average Service time for Normal Cases at Counter 1 : %4.5f\n',normal_avgST);
    end
    
    if normal_counter2Num ~= 0
        normal_avgST = 0;
        for i = 1:normal_counter2Num
            normal_avgST = normal_avgST+normal_serviceTimeCounter2(i);
        end
        normal_avgST = normal_avgST/normal_counter2Num;
        printf('Average Service time for Normal Cases at Counter 2 : %4.5f\n',normal_avgST);
    end
    
    if priorityPatientNum ~= 0
        priority_avgST = 0;
        for i = 1:priorityPatientNum
            priority_avgST = priority_avgST+priority_patientST(i);
        end
        priority_avgST = priority_avgST/priorityPatientNum;
        printf('Average Service time for Priority Cases (Counter 3) : %4.5f\n',priority_avgST);
    end
    
    printf('==============================================================\n');
    printf('===                   END DATA REPORT                      ===\n');
    printf('==============================================================\n\n');

    printf('*************************LOGS*************************\n');
    printf('##In operation are normal case counter %4.0f,%4.0f\n',normalCounterNum1,normalCounterNum2);
    printf('##In operation are priority case counter %4.0f\n',priority_counterNum2);
    
    clock = -1;
    for i = 1:5000
        clock=clock+1;
        for j = 1:patientNumber
            if patientArrMain(j) == clock
                printf('Patient%4.0f arrives at minute%4.0f and the queue number is %4.0f\n',j,clock,patientQueueNumberMain(j));
            end
            if patientDepartMain(j) == clock
                printf('Patient%4.0f departed at minute%4.0f\n',j,clock);
            end
            if patientServiceMain(j) == clock
                printf('Patient%4.0f service started at minute %4.0f at counter %4.0f with queue number of %4.0f\n',j,clock,patientCounterNumberMain(j),patientQueueNumberMain(j));
            end
        end
    end
    printf('\n\n*************************END OF LOGS*************************\n');